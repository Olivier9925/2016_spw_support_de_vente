<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Private access : reset password</title>
<link href="ap/style/formulaires.css" rel="stylesheet" type="text/css">
<link href="ap/style/style.css" rel="stylesheet" type="text/css">
</head>

<body>

	
	<?php
	if ( isset ( $_GET['user_email'] ) &&isset ( $_GET['user_firstName'] ) &&isset ( $_GET['user_lastName'] ) &&isset ( $_GET['user_organisation'] ) ) 
	{ 
		//$_SESSION['user_email'] = $_GET['user_email']; 
		//echo $_SESSION['user_email'];
		$to = 'communication@spineway.com';
		$objet = '[Private access] : forgotten password';
		$contenu_mail = '<br><br>Demande de nouveau password pour '.$_GET['user_firstName'].' '.$_GET['user_lastName'].'<br><br>';
		$contenu_mail .= 'eMail : '.$_GET['user_email'].'<br>';
		$contenu_mail .= 'Soci&eacute;t&eacute; : '.$_GET['user_organisation'].'<br>';


		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: PRIVATE ACCESS<communication@spineway.com>' . "\r\n";

		
		mail($to, $objet, $contenu_mail , $headers);
		echo '<center><br><br><br>Your request will be treated as soon as possible. Thank you</center>';
		header( "refresh:5;url=/web" );
	}
	else
	{
		?>
		<div style="position: relative; top: 50px;">
			<center>
			  <form method="get" action="reset_password.php">
				<input type="text" name="user_email" placeholder="eMail" class="input_text_gros" required><br>
				<input type="text" name="user_firstName" placeholder="first name" class="input_text_gros" required><br>
				<input type="text" name="user_lastName" placeholder="last name" class="input_text_gros" required><br>
				<input type="text" name="user_organisation" placeholder="Company" class="input_text_gros" required><br>
				<button class="bouton_cyan bouton_petit">OK</button>
			  </form>
			</center>
		</div>
		<?php
	}
	?>
	
	
	
</body>
</html>