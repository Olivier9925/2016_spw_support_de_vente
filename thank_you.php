<!doctype html>
<html>

<head>
	
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109888599-1"></script>
			<script>
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag('js', new Date());

			  gtag('config', 'UA-109888599-1');
			</script>
			<!-- Global site tag (gtag.js) - Google Analytics -->
	
	
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	
	<link href="https://fonts.googleapis.com/css?family=Hind|Poppins|Roboto" rel="stylesheet">
	<link href="ap/style/style.css" rel="stylesheet" type="text/css">
	<link href="ap/style/formulaires.css" rel="stylesheet" type="text/css">
	<link href="ap/style/dossier.css" rel="stylesheet" type="text/css">
	<link href="ap/style/form.css" rel="stylesheet" type="text/css">
	
	
<link rel="apple-touch-startup-image" href="ap/images/icone_spw.png">
	<link rel="apple-touch-icon" href="ap/images/icone_spw.png"/>
	<title>Spineway Private Access</title>
</head>
<body>
	
	Thank you, 
</body>
</html>