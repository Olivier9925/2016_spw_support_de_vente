<?php

	function generateXML($q, $name, $resourceId, $display = false){

		include "dbb_connect.php";

		//Execute the query
		$result = mysqli_query($link,$q);
		$data = "";

		//XML HEADER
		$data .= '<?xml version="1.0" encoding="utf-8"?>'."\n";
		$data .= '	<Table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'."\n";
		$data .= '		<OccurrenceList>'."\n";


		//For Each occurrence
		$i = 0;
		while($row = mysqli_fetch_assoc($result)){
			$data .= "			<Occurrence id='".$i."'>\n";
			$data .= "				<hashtable>\n";
			$data .= "					<fields>\n";
			foreach($row as $field => $value) {
				$data .= "						<field>\n";
				$data .= "							<key>".$field."</key>\n";
				$data .= "							<value>".$value."</value>\n";
				$data .= "						</field>\n";
			}
			$data .= "					</fields>\n";
			$data .= "				</hashtable>\n";
			$data .= "			</Occurrence>\n";
			$i++;
		}

		//XML FOOTER
		$data .= "		</OccurrenceList>\n";
		$data .= "	</Table>";

		//Minify
		file_put_contents("../data/".$resourceId."/".$name.".xml", $data);

		if($display) echo $data;

	}

	function updateMd5($resourceId = -1){

		include "dbb_connect.php";

		//FILLMD5
		$result = mysqli_query($link,"SELECT * FROM resource, resourceType WHERE resourceType_id = resource_resourceType_id AND resource_disabled = 0");

		while($row = mysqli_fetch_assoc($result)){

			$id = $row['resource_id'];

			if($resourceId == -1 || $id == $resourceId){
				$filename = $row["resource_filename"];

				$url = getDataUrl();
				$file = $id."/".$filename;
				// $md5 = md5_file($url.$file);
				$md5 = md5_file("../data/$file");

				if($md5 != $row['resource_md5_hash']){
					$result2 = mysqli_query($link,"UPDATE resource SET resource_md5_hash = '".$md5."' WHERE resource_id = ".$id);
				}
			}




		}
	}

	function getDataUrl(){

		$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";

		if ($_SERVER["SERVER_PORT"] != "80"){
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}else{
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}

		$url = "";
		$array = explode("/", $pageURL);
		for($i=0; $i<count($array)-2;$i++){
			$url .= $array[$i]."/";
		}

		return $url."data/";

	}

	function newGuid(){
		if (function_exists('com_create_guid') === true)  return trim(com_create_guid(), '{}');
		else											  return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}

	function getUrl() {
		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
		 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		}else{
		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		 return $pageURL;
	}

	function getUrlParentFolder() {
		$url = getUrl();
		return str_replace(basename($url), "", $url);
	}

	function mailTo($recipient, $subject, $content){
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'my.odolium.com/mailto/validation',
			CURLOPT_USERAGENT => 'cURL Request',
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => array(
											'content' => $content,
											'recipient' => $recipient,
											'subject' => $subject
			)
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
	}

?>
