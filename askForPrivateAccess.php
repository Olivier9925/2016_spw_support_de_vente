<?php

	require_once "include/dbb_connect.php" ;
	require_once "include/checkHash.php";	
	include "include/functions.php";

	$userId = $_POST["userId"];
	$userFirstName = $_POST['firstName'];
	$userLastName = $_POST['lastName'];
	$userPhone = $_POST['phone'];
	$userEmail = $_POST['email'];
	$userMessage = $_POST['message'];
	
	
	$query = "UPDATE user SET `user_request` = 1, `user_message` = '".$userMessage."' WHERE user_id = ".$userId;
	$result = mysqli_query($link,$query);
	
	$emailSalesman = "";
	/*if($userCountry > 0){
		$result = mysqli_query($link,"SELECT * FROM country, user WHERE country_salesman_user_id = user_id AND country_id = ".$userCountry);
		if(mysqli_num_rows($result) > 0){
			$line = mysqli_fetch_assoc($result);
			$emailSalesman = $line["user_email"];
		}
	}*/
	

	//On envoi un mail au admin. 
	$result = mysqli_query($link,"SELECT user_email FROM user WHERE user_profile_id = 1 AND user_active = 1");
	
		if(mysqli_num_rows($result) > 0){
			
			$i = 0;
			while ($line = mysqli_fetch_assoc($result)){
			
				$langId = isset($_POST["langId"]) ? $_POST["langId"]: 2;
				$lnk_grant = getUrlParentFolder()."privateAccess.php?langId=".$langId."&request=1&token=".$userId;
				$lnk_deny = getUrlParentFolder()."privateAccess.php?langId=".$langId."&request=0&token=".$userId;
				$lnk_backoffice_user = "https://spineway.odolium.com/bo/index.php?page=user&user_id=".$userId;
			
				switch($langId){
					case 1 : 
						$subject = "Nouvelle demande pour un accès distributeur";
						$content = "Bonjour,<br/><br/> ".$userFirstName." ".$userLastName." a demandé un accès distributeur.<br/><br/>Ci-dessous ses informations personnelles :<br/>";
						$content .= "Email : ".$userEmail."<br/>";
						if($userPhone != "")	$content .= "Téléphone : ".$userPhone."<br/>";
						$content .= "Message : ".$userMessage."<br/>";

						$content .= "<br/>";
						$content .= "<br/>";

						$content .= "Veuillez appuyer sur ce lien pour valider la demande : <a href=".$lnk_grant.">".$lnk_grant."</a><br/><br/>";
						$content .= "Veuillez appuyer sur ce lien pour refuser la demande : <a href=".$lnk_deny.">".$lnk_deny."</a><br/><br/>";
						$content .= "Ici vous pouvez modifier les informations concernant l'utilisateur :<a href=".$lnk_backoffice_user.">".$lnk_backoffice_user."</a><br/><br/>";
						
						$content .= "Cordialement,<br/><br/>";
						$content .= "L'équipe Spineway";
						
						break;
					case 2 : 
					case 4 : 
						$subject = "New request of distributor access (upgrade)";
						$content = "Hello,<br/><br/> ".$userFirstName." ".$userLastName." has requested an upgrade of his account.<br/><br/>Below are his personal info:<br/>";
						$content .= "Email : ".$userEmail."<br/>";
						if($userPhone != "")	$content .= "Téléphone : ".$userPhone."<br/>";
						$content .= "Message : ".$userMessage."<br/>";

						$content .= "<br/>";
						$content .= "<br/>";

						$content .= "Please click on this link to approve the request: <a href=".$lnk_grant.">".$lnk_grant."</a><br/><br/>";
						$content .= "Please click on this link to deny the request: <a href=".$lnk_deny.">".$lnk_deny."</a><br/><br/>";
						$content .= "You can edit the user information here: <a href=".$lnk_backoffice_user.">".$lnk_backoffice_user."</a><br/><br/>";
						
						$content .= "Best regards,<br/><br/>";
						$content .= "The Spineway Team";
						break;
					case 3 : 
						$subject = "Nueva solicitud de acceso de distribuidor (promoción)";
						$content = "Hola,<br/><br/> ".$userFirstName." ".$userLastName." solicita actualizar su cuenta.<br/><br/>A continuación se detallan sus datos personales : <br/>";
						$content .= "Correo electrónico : ".$userEmail."<br/>";
						if($userPhone != "")	$content .= "Número de teléfono : ".$userPhone."<br/>";
						$content .= "Mensaje : ".$userMessage."<br/>";

						$content .= "<br/>";
						$content .= "<br/>";

						$content .= "Por favor, haga clic en este enlace para confirmar : <a href=".$lnk_grant.">".$lnk_grant."</a><br/><br/>";
						$content .= "Por favor, haga clic en este enlace para rechazar : <a href=".$lnk_deny.">".$lnk_deny."</a><br/><br/>";
						$content .= "Se puede revisar los datos del usuario aquí : <a href=".$lnk_backoffice_user.">".$lnk_backoffice_user."</a><br/><br/>";
						
						$content .= "Un Saludo,<br/><br/>";
						$content .= "El equipo Spineway";
						break;
				}
				
				

				$recipient = $line['user_email'];

				// mailTo("remi.chapelain@gmail.com", $subject, $content);
				mailTo($recipient, $subject, $content);
				if($emailSalesman != "" && $i == 0)	mailTo($emailSalesman, $subject, $content);
				$i++;
			}
			
			echo "mailed";
			
		}else{
			echo "noAdmin";
		}


	
?>
