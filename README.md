# Support de vente et base documentaire.
Création d'un outil pour la société Spineway


 - responsive
 - Disponible en 3 langues
 - Accès sécurisé avec gestion des autorisations pour les différents types d'utilisateurs :
 
    Commerciaux (Intégralité du contenu)
    
    Prospects (Uniquement les brochures documentation institutionnelle)
    
    Distributeurs (Brochures et supports de vente, videos, versions imprimables...)
    
    Chirugiens (Brochures, Techniques opératoires, videos bloc opératoire...)
    
    Accès temporaires avec choix de la durée
    
 
## https://www.spineway.support


<p align="center">
  
  <img src="https://github.com/Olivier9925/spw_support_de_vente/blob/master/CAPTURE_ECRAN/screencapture-app-spineway-support-web-V2-2019-11-23-21_39_29.png?raw=true" width="800">
<hr />
  <img src="https://github.com/Olivier9925/spw_support_de_vente/blob/master/CAPTURE_ECRAN/screencapture-app-spineway-support-web-V2-2019-11-23-21_38_20.png?raw=true" width="800">
  <hr />
  <img src="https://github.com/Olivier9925/spw_support_de_vente/blob/master/CAPTURE_ECRAN/screencapture-app-spineway-support-web-V2-2019-11-23-21_38_34.png?raw=true" width="800">
  <hr />
  <img src="https://github.com/Olivier9925/spw_support_de_vente/blob/master/CAPTURE_ECRAN/screencapture-app-spineway-support-web-V2-2019-11-23-21_39_06.png?raw=true" width="800">
  
  
  
  </p>
