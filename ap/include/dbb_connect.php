<?php

	$host = 'localhost';
	$user = '/*/*/*/*/*/*/';
	$password = '/*/*/*/*/*/*/';

	$database = 'myspineway';

	$link = mysqli_connect($host, $user, $password, $database) or die("Error " . mysqli_error($link));

	mysqli_query($link, "SET NAMES 'utf8'");

	// forbids multiple function declaration
	if (!function_exists('debugQuery')) {
		// print query for normal and prepared statement
		function debugQuery($query, $param=[]) {
			// normalquery
			if (count($param) == 0) {
				return "$query";
			}

			// param nb
			$num = substr_count($query, '?');
			// if param nb mach "?" nb in query
			if ($num == count($param)) {
				foreach ($param as $value) {
					// same as str_replace but only first occurence
					$query = preg_replace('/\?/', json_encode($value), $query, 1);
				}
				return "$query";
			} else {
				return "Number of param not matching, having ".count($param)." param(s) when expecting ".$num." in query";
			}
		}
	}


?>
